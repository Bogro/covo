<?php

namespace App\DataFixtures;

use App\Entity\Account;
use App\Entity\User;
use App\Entity\Wallet;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{

    protected $encoder;

    /**
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername("user225");
        $user->setRoles(['ROLE_ADMIN']);
        $user->setPassword(
            $this->encoder->encodePassword(
                $user,
                "user225"
            )
        );

        $manager->persist($user);
        $manager->flush();

        $account = new Account();
        $account->setUsers($user);

        $manager->persist($account);
        $manager->flush();

        $wallet = new Wallet();
        $wallet->setAccount($account);
        $wallet->setAmount(1000.0);
        $wallet->setCreatedAt(new \DateTimeImmutable());

        $manager->persist($wallet);
        $manager->flush();
    }
}
