<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TripRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"get:trip", "put:trip"}},
 * )
 * @ORM\Entity(repositoryClass=TripRepository::class)
 */
class Trip
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     * @Groups({"get:trip", "get:account", "get:ticket"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Account::class, inversedBy="trips")
     * @Groups({"get:trip"})
     */
    private $organizer;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get:trip", "put:trip", "get:ticket"})
     */
    private $startingPoint;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get:trip", "put:trip", "get:ticket"})
     */
    private $endPoint;

    /**
     * @ORM\Column(type="date")
     * @Groups({"get:trip", "put:trip", "get:ticket"})
     */
    private $dateOfDeparture;

    /**
     * @ORM\Column(type="time")
     * @Groups({"get:trip", "put:trip", "get:ticket"})
     */
    private $departureTime;

    /**
     * @ORM\Column(type="json")
     * @Groups({"get:trip", "put:trip"})
     */
    private $state = [];

    /**
     * @ORM\Column(type="integer")
     * @Groups({"get:trip", "put:trip"})
     */
    private $numberOfPlaces;

    /**
     * @ORM\Column(type="float")
     * @Groups({"get:trip", "put:trip", "get:ticket"})
     */
    private $price;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @Groups({"get:trip"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @Groups({"get:trip"})
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity=Ticket::class, mappedBy="trip")
     * @Groups({"get:trip"})
     */
    private $tickets;

    /**
     * @ORM\ManyToOne(targetEntity=Car::class, inversedBy="trip")
     * @Groups({"get:trip", "get:ticket"})
     */
    private $car;

    public function __construct()
    {
        $this->tickets = new ArrayCollection();
    }

    public function getId(): ?String
    {
        return (String) $this->id;
    }

    public function getOrganizer(): ?Account
    {
        return $this->organizer;
    }

    public function setOrganizer(?Account $organizer): self
    {
        $this->organizer = $organizer;

        return $this;
    }

    public function removeTraveler(User $traveler): self
    {
        $this->travelers->removeElement($traveler);

        return $this;
    }

    public function getStartingPoint(): ?string
    {
        return $this->startingPoint;
    }

    public function setStartingPoint(string $startingPoint): self
    {
        $this->startingPoint = $startingPoint;

        return $this;
    }

    public function getEndPoint(): ?string
    {
        return $this->endPoint;
    }

    public function setEndPoint(string $endPoint): self
    {
        $this->endPoint = $endPoint;

        return $this;
    }

    public function getDateOfDeparture(): ?\DateTimeInterface
    {
        return $this->dateOfDeparture;
    }

    public function setDateOfDeparture(\DateTimeInterface $dateOfDeparture): self
    {
        $this->dateOfDeparture = $dateOfDeparture;

        return $this;
    }

    public function getDepartureTime(): ?\DateTimeInterface
    {
        return $this->departureTime;
    }

    public function setDepartureTime(\DateTimeInterface $departureTime): self
    {
        $this->departureTime = $departureTime;

        return $this;
    }

    public function getState(): ?array
    {
        return $this->state;
    }

    public function setState(array $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getNumberOfPlaces(): ?int
    {
        return $this->numberOfPlaces;
    }

    public function setNumberOfPlaces(int $numberOfPlaces): self
    {
        $this->numberOfPlaces = $numberOfPlaces;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|Ticket[]
     */
    public function getTickets(): Collection
    {
        return $this->tickets;
    }

    public function addTicket(Ticket $ticket): self
    {
        if (!$this->tickets->contains($ticket)) {
            $this->tickets[] = $ticket;
            $ticket->setTrip($this);
        }

        return $this;
    }

    public function removeTicket(Ticket $ticket): self
    {
        if ($this->tickets->removeElement($ticket)) {
            // set the owning side to null (unless already changed)
            if ($ticket->getTrip() === $this) {
                $ticket->setTrip(null);
            }
        }

        return $this;
    }

    public function getCar(): ?Car
    {
        return $this->car;
    }

    public function setCar(?Car $car): self
    {
        $this->car = $car;

        return $this;
    }
}
