<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CarRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource
 * @ORM\Entity(repositoryClass=CarRepository::class)
 */
class Car
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     * @Groups({"get:account", "get:trip"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get:trip"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"get:trip"})
     */
    private $regisNumber;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $numberOfPlaces;

    /**
     * @ORM\Column(type="json")
     * @Groups({"get:trip", "get:ticket"})
     */
    private $typeCar = [];

    /**
     * @ORM\ManyToOne(targetEntity=Account::class, inversedBy="cars")
     */
    private $owner;

    /**
     * @ORM\OneToMany(targetEntity=Trip::class, mappedBy="car")
     */
    private $trip;

    /**
     * @ORM\OneToMany(targetEntity=Remark::class, mappedBy="car")
     * @Groups({"get:trip"})
     */
    private $remarks;

    public function __construct()
    {
        $this->trip = new ArrayCollection();
        $this->remarks = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return (string) $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getRegisNumber(): ?int
    {
        return $this->regisNumber;
    }

    public function setRegisNumber(int $regisNumber): self
    {
        $this->regisNumber = $regisNumber;

        return $this;
    }

    public function getNumberOfPlaces(): ?string
    {
        return $this->numberOfPlaces;
    }

    public function setNumberOfPlaces(string $numberOfPlaces): self
    {
        $this->numberOfPlaces = $numberOfPlaces;

        return $this;
    }

    public function getTypeCar(): ?array
    {
        return $this->typeCar;
    }

    public function setTypeCar(array $typeCar): self
    {
        $this->typeCar = $typeCar;

        return $this;
    }

    public function getOwner(): ?Account
    {
        return $this->owner;
    }

    public function setOwner(?Account $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return Collection|Trip[]
     */
    public function getTrip(): Collection
    {
        return $this->trip;
    }

    public function addTrip(Trip $trip): self
    {
        if (!$this->trip->contains($trip)) {
            $this->trip[] = $trip;
            $trip->setCar($this);
        }

        return $this;
    }

    public function removeTrip(Trip $trip): self
    {
        if ($this->trip->removeElement($trip)) {
            // set the owning side to null (unless already changed)
            if ($trip->getCar() === $this) {
                $trip->setCar(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Remark[]
     */
    public function getRemarks(): Collection
    {
        return $this->remarks;
    }

    public function addRemark(Remark $remark): self
    {
        if (!$this->remarks->contains($remark)) {
            $this->remarks[] = $remark;
            $remark->setCar($this);
        }

        return $this;
    }

    public function removeRemark(Remark $remark): self
    {
        if ($this->remarks->removeElement($remark)) {
            // set the owning side to null (unless already changed)
            if ($remark->getCar() === $this) {
                $remark->setCar(null);
            }
        }

        return $this;
    }
}
